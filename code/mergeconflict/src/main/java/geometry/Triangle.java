package geometry;

public class Triangle {
 private double base;
 private double height;
 
 public Triangle(double base, double height){
    if(base <= 0 || height <=0){
        throw new IllegalArgumentException("Dimensions can't be less than or equal to 0");
    }
    this.base = base;
    this.height = height;
 }

 public double getArea(){
    double area = 0.5 * base * height;
    return area;
 }

 public String toString(){
    String result;
    result = "" + this.base+ " " + this.height;
    return result;
 }
}
