package geometry;

public class Shapes {
    public static void main(String[] args){
        Triangle tri = new Triangle(5.0, 6.0);
        System.out.println(tri + " Area: " + tri.getArea());
        
        Square square = new Square(8);

        System.out.println(square + " Area: " + square.getArea());

    }
}
