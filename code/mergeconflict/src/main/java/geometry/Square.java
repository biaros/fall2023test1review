package geometry;

public class Square {
    private double side;

    public Square(double side)
    {
        if (side <= 0)
        {
            throw new IllegalArgumentException("side cannot be less than or equal to 0");
        }
        this.side = side;
    }

    public double getSide()
    {
        return(this.side);
    }

    public double getArea()
    {
        return(this.side * this.side);
    }

    public String toString()
    {
        String s = "this square is a " + this.side + " x " + this.side + " square.";
        return(s);
    }
}
