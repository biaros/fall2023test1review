package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

public class SquareTest {

    @Test(expected = IllegalArgumentException.class)
    public void shouldReturnException_constructorNegativeSide()
    {
        Square s = new Square(-4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldReturnException_constructorZeroSide()
    {
        Square s = new Square(0);
    }

    @Test
    public void shoudlReturn4_getSide()
    {
        Square s = new Square(4);
        assertEquals(4, s.getSide(), 0.0000000001);
    }

    @Test
    public void shouldReturn100_getArea()
    {
        Square s = new Square(10);
        assertEquals(100, s.getArea(), 0.0000000001);
    }

    @Test
    public void shouldReturnToString()
    {
        Square s = new Square(10);
        String expected = "this square is a " + 10.0 + " x " + 10.0 + " square.";
        assertEquals(expected, s.toString());
    }
}
