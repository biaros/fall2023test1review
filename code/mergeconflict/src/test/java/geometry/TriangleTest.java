package geometry;
import static org.junit.Assert.*;
import org.junit.Test;

public class TriangleTest {
    @Test(expected = IllegalArgumentException.class)
    public void exceptionForNegativeInputs(){
        Triangle tri = new Triangle(-3, -6);

        }
    @Test(expected = IllegalArgumentException.class)
    public void exceptionForZeroesInput(){
        Triangle tri = new Triangle(0,1);
        }

    @Test
    public void testingArea(){
        Triangle tri = new Triangle(3,6);
        assertEquals(9, tri.getArea(), 0.00000001);
        }

    @Test
    public void testingTritoString(){
        Triangle tri = new Triangle(4,8);
        String expected = "4.0 8.0";
        assertEquals(expected, tri.toString());
        }

    }

